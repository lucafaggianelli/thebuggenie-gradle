package com.tesladocet.thebuggenie;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.IconTextView;
import android.widget.TextView;

/**
 * A fragment representing a single Issue detail screen. This fragment is either
 * contained in a {@link IssueListFragmentMain} in two-pane mode (on tablets) or a
 * {@link IssueDetailActivity} on handsets.
 */
public class IssueDetailFragment extends Fragment {
	
	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_ID = "item_id";

	String projectKey;
	int issueId;
	
	TextView id;
	TextView title;
	TextView description;
	
	TextView type;
	IconTextView typeIcon;
	
	TextView priority;
	IconTextView priorityIcon;
	
	TextView reproduction;
	TextView createdBy;
	TextView assignedTo;
	
	TBGIssue issue;
	
	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public IssueDetailFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getArguments().containsKey(ARG_ITEM_ID)) {
			issueId = getArguments().getInt(ARG_ITEM_ID);
		}
		
		if (getArguments().containsKey("prj")) {
			projectKey = getArguments().getString("prj");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.fragment_issue_detail,
				container, false);

		return rootView;
	}
	
	@Override
	public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		TBG.getInstance().getIssue(projectKey, issueId, new TBGCallback<TBGIssue>() {
			@Override
			public void onSuccess(TBGIssue result) {
				issue = result;
				setupView(view);
			}
			
			@Override
			public void onError() {}
		});
	}
	
	private void setupView(View view) {
		id = (TextView) view.findViewById(R.id.id);
		title = (TextView) view.findViewById(R.id.title);
		description = (TextView) view.findViewById(R.id.description);
		type = (TextView) view.findViewById(R.id.type);
		typeIcon = (IconTextView) view.findViewById(R.id.type_icon);
		priority = (TextView) view.findViewById(R.id.priority);
		priorityIcon = (IconTextView) view.findViewById(R.id.priority_icon);
		reproduction = (TextView) view.findViewById(R.id.reproduction);
		createdBy = (TextView) view.findViewById(R.id.created_by);
		assignedTo = (TextView) view.findViewById(R.id.assigned_to);
		
		id.setText("#" + issue.id);
		title.setText(issue.title);
		description.setText(issue.description);
		
		createdBy.setText(issue.postedBy);
		assignedTo.setText(issue.assignedTo);
		
		type.setText(issue.issueNo.split(" #")[0]);
		typeIcon.setText("{fa_bug}");
		
		priority.setText(issue.priority);
		priorityIcon.setText(issue.getPriorityResource());
		priorityIcon.setTextColor(Color.RED);
		
		//reproduction.setText(issue.);
	}
}