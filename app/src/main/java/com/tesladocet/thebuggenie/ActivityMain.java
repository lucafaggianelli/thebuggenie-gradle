package com.tesladocet.thebuggenie;

import com.tesladocet.thebuggenie.ui.DrawerItem;
import com.tesladocet.thebuggenie.ui.DrawerItems;
import com.tesladocet.thebuggenie.ui.DrawerItems.ProjectItem;
import com.tesladocet.thebuggenie.ui.DrawerItems.TextItem;
import com.tesladocet.thebuggenie.ui.NavigationDrawerFragment;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;

public class ActivityMain extends ActionBarActivity implements
		NavigationDrawerFragment.NavigationDrawerCallbacks {

	private final static String KEY_CURRENT_PROJECT = "current_project";
	
	/**
	 * Fragment managing the behaviors, interactions and presentation of the
	 * navigation drawer.
	 */
	private NavigationDrawerFragment navigationDrawer;

	TBG tbg;
	public static Resources res;
	
	ActionBar actionBar;
	boolean firstDrawerEvent = false;
	SharedPreferences preferences;
	Loader mainLoader;
    ProjectItem drawerItemProject;
	
	// Projects
	TBGProject[] projects = new TBGProject[]{};
	String[] projectsNames;
	TBGProject currentProject = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		actionBar = getSupportActionBar();
		res = getResources();
		preferences = getPreferences(MODE_PRIVATE);
		
		tbg = TBG.newInstace(this);
		
		if (tbg.isConfigured()) {
			setupProjects();
			startService(new Intent(this, NotificationService.class));
		} else {
			startActivityForResult(new Intent(this, SettingsActivity.class),
					SettingsActivity.REQUEST_SERVER_SETUP);
		}
		
		navigationDrawer = (NavigationDrawerFragment) getSupportFragmentManager()
				.findFragmentById(R.id.navigation_drawer);
		
		actionBar.setTitle(getTitle());
		
		// Set up the drawer.
		navigationDrawer.setUp(R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));

        drawerItemProject = new ProjectItem(this);
        navigationDrawer.addItems(drawerItemProject);
		
		mainLoader = new Loader(findViewById(R.id.container), 1) {
			@Override
			void onFinish(boolean failed) {
                if (!failed) {
                    currentProject = TBGProject.findById(projects,
                            preferences.getString(KEY_CURRENT_PROJECT, null));

                    if (currentProject != null) {
                        actionBar.setTitle(currentProject.name);
                    }

                    fillDrawer();
                }
			}
		};
        mainLoader.getLoading().setText("Loading projects...");
        mainLoader.getLoading().setTextFail("Can't connect to TBG!");
	}

	@Override
	protected void onResume() {
		super.onResume();
	}
	
	public void pickProjectsDialog(View v) {
	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setTitle("Your projects")
	    		.setItems(projectsNames, new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int which) {
	            	   currentProject = projects[which];
	            	   preferences.edit().putString(KEY_CURRENT_PROJECT,
	            			   currentProject.id).apply();

                       drawerItemProject.setProject(currentProject);
                       navigationDrawer.adapter.notifyDataSetChanged();
	            	   
	            	   actionBar.setTitle(currentProject.name);
	            	   actionBar.setSubtitle(null);
	               }
	           });
	    builder.create().show();
	}
	
	private void setupProjects() {
		tbg.getProjects(new TBGCallback<TBGProject[]>() {
            @Override
            public void onSuccess(TBGProject[] result) {
                projects = result;
                projectsNames = new String[result.length];
                for (int i = 0; i < result.length; i++) {
                    projectsNames[i] = result[i].name;
                }
                mainLoader.taskDone();
            }

            @Override
            public void onError() {
                mainLoader.taskDone(true);
            }
        });
	}

    private void fillDrawer() {
        navigationDrawer.addItems(new DrawerItem[]{
                new TextItem(R.string.drawer_issues,
                        "{fa_bug}",
                        IssueListFragmentMain.class),

                new TextItem(R.string.drawer_releases,
                        "{fa_trophy}",
                        null),

                new TextItem(R.string.drawer_planning,
                        "{fa_calendar}",
                        null),

                new TextItem(R.string.drawer_roadmap,
                        "{fa_road}",
                        null),

                new TextItem(R.string.drawer_team,
                        "{fa_users}",
                        null),

                new TextItem(R.string.drawer_statistics,
                        "{fa_area_chart}",
                        null),

                new TextItem(R.string.drawer_timeline,
                        "{fa_clock_o}",
                        null),

                new TextItem(R.string.drawer_release_center,
                        "{fa_cubes}",
                        null),

                new TextItem(R.string.drawer_settings,
                        "{fa_cogs}",
                        null)
        });
    }

	@Override
	protected void onActivityResult(int req, int res, Intent data) {
		if (req == SettingsActivity.REQUEST_SERVER_SETUP &&
				res == SettingsActivity.RESULT_SERVER_SETUP) {
			setupProjects();
			startService(new Intent(this, NotificationService.class));
		}
	}
	
	@Override
	public boolean onNavigationDrawerItemSelected(DrawerItem item, 
			int position) {
		
		if (position == 0)
			return true;
		
		if (currentProject != null) {
			Bundle bundle = new Bundle();
			bundle.putString("prj", currentProject.id);
			navigationDrawer.setFragmentArgument(bundle);
			
			if (item instanceof TextItem) {
				actionBar.setTitle(((TextItem) item).title);
				actionBar.setSubtitle(currentProject.name);
			}
		}
		
		return false;
	}

	public void restoreActionBar() {
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		//actionBar.setTitle(mTitle);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!navigationDrawer.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
			getMenuInflater().inflate(R.menu.activity_project, menu);
			restoreActionBar();
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			startActivity(new Intent(this, SettingsActivity.class));
			return true;
		} else if (id == R.id.notify_test) {
			startService(new Intent(this, NotificationService.class));
		}
		return super.onOptionsItemSelected(item);
	}
}
