package com.tesladocet.thebuggenie;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Resources;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;

public class TBG implements OnSharedPreferenceChangeListener {
	private final static String USER_AGENT = "TBGAndroid/1.0";
	private final static String API_REST_PATH = "api-rest";

	private final static int GET = 0;
	private final static int POST = 1;
	
	private String server = null;
	private static String username = null;
	private static String authKey = null;
	String format = "json";
	
	static TBG myself = null;
	Context context;
	SharedPreferences preferences;
	
	Gson gson;
	private static AndroidHttpClient client = null;
	private static AsyncHttpClient client2;
    private PersistentCookieStore cookieStore;
	
	// API info
	int[] tbgVersion = new int[] {3,0,0};
	int apiVersion = 0;
	boolean restApiModule = false;
	
	public static TBG getInstance() {
		return myself;
	}
	
	public static TBG newInstace(Context c) {
		myself = new TBG(c);
		return myself;
	}
	
	private TBG(Context c) {
		context = c;
		gson = new Gson();

        client2 = new AsyncHttpClient();
        cookieStore = new PersistentCookieStore(context);
        client2.setCookieStore(cookieStore);

		preferences = PreferenceManager.getDefaultSharedPreferences(context);
		preferences.registerOnSharedPreferenceChangeListener(this);
		
		server = preferences.getString(SettingsActivity.KEY_SERVER, null);
		username = preferences.getString(SettingsActivity.KEY_USER, null);
		authKey = preferences.getString(SettingsActivity.KEY_PASSWORD, null);
		
		if (isConfigured()) {
            client2.addHeader("Cookie", buildCookieHeader());
			getInfo();
		}
	}
	
	public boolean isConfigured() {
		return (server != null) &&
				(username != null) &&
				(authKey != null);
	}

	private String buildUrl(String... parts) {
		if (server == null) {
			Log.e("TBG", "Server not configured. "+parts[0]);
			return null;
		}
		
		StringBuilder sb = new StringBuilder(server);

        if (restApiModule) {
            sb.append("/");
            sb.append(API_REST_PATH);
        }

		for (String p: parts) {
			sb.append("/");
			sb.append(p);
		}
		
		return sb.toString();
	}

	private String buildCookieHeader() {
		if (username == null || authKey == null)
			return null;
		
		return String.format(Locale.US, "tbg3_username=%s; tbg3_password=%s",
				username, authKey);
	}
	
	private JsonElement getJson(String content) {
		try {
			return new JsonParser().parse(content);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void parseForm(ViewGroup vg) {
		View v;
		for (int i = 0; i < vg.getChildCount(); i++) {
			v = vg.getChildAt(i);
			v.getId();
			
			if (v instanceof EditText) {
				((EditText) v).getText().toString();
			} else if (v instanceof Spinner) {
				((Spinner) v).getSelectedItemId();
			}
		}
	}
	
	private int[] ver2int(String str) {
		int[] version = new int[] {0};
		String[] versionS;
		
		if (str != null && (versionS = str.split(".")) != null) {
			version = new int[versionS.length];
            Log.d("TBG", "Splitting "+str+" is long: "+versionS.length);
			for (int i=0; i < versionS.length; i++) {
				version[i] = Integer.parseInt(versionS[i]);
			}
		}
		
		return version;
	}
	
	private void getInfo() {
		client2.get(buildUrl(API_REST_PATH), new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers,
					JSONObject response) {
                String tmp = response.optString("tbgVersion","3.0.0.0");
				tbgVersion = ver2int(tmp);
				apiVersion = Integer.parseInt(response.optString("apiVersion", "0"));
				restApiModule = apiVersion > 0;
			}
		});
	}
	
	public void login(final String user, String pass,
			final TBGCallback<Boolean> callback) {

        RequestParams params = new RequestParams();
        params.put("tbg3_username", user);
        params.put("tbg3_password", pass);

        client2.post(buildUrl(API_REST_PATH, "do", "login"), params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] data) {
                for (Header h: headers) {
                    for (HeaderElement e: h.getElements()) {
                        if (e.getName().equals("tbg3_password")) {
                            if (!e.getValue().equals("deleted")) {
                                preferences.edit().putString(SettingsActivity.KEY_USER, user);
                                preferences.edit().putString(SettingsActivity.KEY_PASSWORD, authKey);
                                preferences.edit().apply();
                                callback.onSuccess(true);
                            } else {
                                callback.onError();
                            }
                            return;
                        }
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                callback.onError();
            }
        });
	}

    public void getProjects(final TBGCallback<TBGProject[]> callback) {
        if (restApiModule) {
            client2.get(buildUrl("projects"),
                    new GsonResponse<TBGProject[]>(callback, TBGProject[].class) {});
        } else {
            client2.get(buildUrl("list", "projects", format), new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    callback.onSuccess(TBGProject.fromJson(response));
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    callback.onError();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    callback.onError();
                }
            });
        }
    }
	
	public void getIssues(String projectKey, final TBGCallback<TBGIssue[]> callback) {
        if (restApiModule) {
            client2.get(buildUrl("projects", projectKey, "issues"),
                    new GsonResponse<TBGIssue[]>(callback, TBGIssue[].class) {});
            Log.d("TBG", "Just called "+buildUrl("projects", projectKey, "issues"));
        } else {
            client2.get(buildUrl(projectKey, "list", "issues", format), new BaseJsonHttpResponseHandler<TBGIssue[]>() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, TBGIssue[] response) {
                    callback.onSuccess(response);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, TBGIssue[] errorResponse) {
                    callback.onError();
                }

                @Override
                protected TBGIssue[] parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                    JsonObject i = getJson(rawJsonData).getAsJsonObject()
                            .get("issues")
                            .getAsJsonObject();

                    Map<String, TBGIssue> issues =
                            gson.fromJson(i, new TypeToken<Map<String, TBGIssue>>() {
                            }.getType());

                    return issues.values().toArray(new TBGIssue[]{});
                }
            });
        }
	}
	
	public void getIssue(String projectKey, int id, final TBGCallback<TBGIssue> callback) {
        if (restApiModule) {
            client2.get(buildUrl("projects", projectKey, "issues", ""+id),
                    new GsonResponse<TBGIssue>(callback, TBGIssue.class) {});
        }
	}

    /*
	public void listIssueTypes(final TBGCallback<String[]> callback) {
		new Task() {
			@Override
			public void onSuccess(String content) {
				String[] types = gson.fromJson(getJson(content), String[].class);
				callback.onSuccess(types);
			}

			@Override
			public void onError(HttpResponse res) {};
		}.execute(buildUrl("list/issuetypes",format));
	}
	
	public void listIssueFields(String pKey, String iType, 
			final TBGCallback<String[]> callback) {
		new Task() {
			@Override
			public void onSuccess(String content) {
				JsonArray fields = getJson(content).getAsJsonObject()
						.get("issuefields")
						.getAsJsonArray();
				
				String[] types = gson.fromJson(fields, String[].class);
				callback.onSuccess(types);				
			}

			@Override
			public void onError(HttpResponse res) {};
		}.execute(buildUrl(pKey, "list/issuefields/for/type",iType,format));
	}
	
	public void listFieldValues(String fieldKey, 
			final TBGCallback<TBGFieldValues> callback) {
		new Task() {
			@Override
			public void onSuccess(String content) {
				TBGFieldValues values = gson.fromJson(getJson(content), 
						TBGFieldValues.class);
				callback.onSuccess(values);
			}

			@Override
			public void onError(HttpResponse res) {};
		}.execute(buildUrl("list/fieldvalues/for/field", fieldKey,format));
	}*/

    class GsonResponse<A> extends BaseJsonHttpResponseHandler<A> {

        TBGCallback<A> callback;
        Class<A> _class;

        public GsonResponse(TBGCallback<A> callback, Class<A> c) {
            this.callback = callback;
            this._class = c;
        }

        @Override
        public void onStart() {
            Log.d("TBG", "Requesting "+getRequestURI().toString());
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, A response) {
            callback.onSuccess(response);
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, A errorResponse) {
            callback.onError();
        }

        @Override
        protected A parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
            return gson.fromJson(getJson(rawJsonData), _class);
        }
    }

	@Override
	public void onSharedPreferenceChanged(SharedPreferences pref, String key) {		
		if (key.equals(SettingsActivity.KEY_SERVER)) {
			server = pref.getString(SettingsActivity.KEY_SERVER, null);
			try {
				new URL(server);
			} catch (MalformedURLException e) {
				e.printStackTrace();
				server = null;
			}
		} else if (key.equals(SettingsActivity.KEY_USER)) {
			username = pref.getString(SettingsActivity.KEY_USER, null);
		} else if (key.equals(SettingsActivity.KEY_PASSWORD)) {
			authKey = pref.getString(SettingsActivity.KEY_PASSWORD, null);
		}
	}
}