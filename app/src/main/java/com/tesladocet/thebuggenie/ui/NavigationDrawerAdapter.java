package com.tesladocet.thebuggenie.ui;

import java.util.ArrayList;

import com.tesladocet.thebuggenie.R;
import com.tesladocet.thebuggenie.ui.DrawerItem.DrawerItemHolder;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class NavigationDrawerAdapter extends ArrayAdapter<DrawerItem> {
	
    Context context;
    ArrayList<DrawerItem> drawerItemList = new ArrayList<DrawerItem>();
    public int selectedItem = -1;

    public NavigationDrawerAdapter(Context context) {
        super(context, R.layout.drawer_item);
        this.context = context;
    }

    @Override
    public int getCount() {
        return drawerItemList.size();
    }

    public void addItem(DrawerItem item) {
        drawerItemList.add(item);
    }

    @Override
    public DrawerItem getItem(int position) {
        return drawerItemList.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).type;
    }

    @Override
    public int getViewTypeCount() {
        return DrawerItem.TYPE_SEARCH + 1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DrawerItemHolder drawerHolder;
        View view = convertView;
        DrawerItem item = getItem(position);

        if (view == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            view = inflater.inflate(item.resource, parent, false);

            drawerHolder = item.getHolder(view);
            view.setTag(drawerHolder);
        } else {
            drawerHolder = (DrawerItemHolder) view.getTag();
        }

        item.getView(drawerHolder, selectedItem == position);

        return view;
    }
}