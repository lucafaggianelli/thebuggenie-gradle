package com.tesladocet.thebuggenie.ui;

import com.squareup.picasso.Picasso;
import com.tesladocet.thebuggenie.ActivityMain;
import com.tesladocet.thebuggenie.IssueListFragmentMain;
import com.tesladocet.thebuggenie.R;
import com.tesladocet.thebuggenie.TBGProject;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.IconTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class DrawerItems {
	
	public static DrawerItem[] get() {
		return new DrawerItem[] {
            new TextItem(R.string.drawer_issues,
                    "{fa_bug}",
                    IssueListFragmentMain.class),

            new TextItem(R.string.drawer_releases,
                    "{fa_trophy}",
                    null),

            new TextItem(R.string.drawer_planning,
                    "{fa_calendar}",
                    null),

            new TextItem(R.string.drawer_roadmap,
                    "{fa_road}",
                    null),

            new TextItem(R.string.drawer_team,
                    "{fa_users}",
                    null),

            new TextItem(R.string.drawer_statistics,
                    "{fa_area_chart}",
                    null),

            new TextItem(R.string.drawer_timeline,
                    "{fa_clock_o}",
                    null),

            new TextItem(R.string.drawer_release_center,
                    "{fa_cubes}",
                    null),

            new TextItem(R.string.drawer_settings,
                    "{fa_cogs}",
                    null),
		};
	}
	
	public static class ProjectItem extends DrawerItem {
        private TBGProject project;
        private Context context;
		private int background = R.drawable.project_background;
		
		public ProjectItem(Context context) {
			this.context = context;
            resource = R.layout.drawer_item_project;
		}

        public void setProject(TBGProject project) {
            this.project = project;
        }

        @Override
		public void getView(DrawerItemHolder holder, boolean sel) {
			Holder h = (Holder) holder;
            h.background.setImageResource(background);
            if (project != null) {
                h.name.setText(project.name);
                if (project.image != null && project.image != "")
                    Picasso.with(context).load(project.image).into(h.logo);
            }
		}

		@Override
		public DrawerItemHolder getHolder(View v) {
			Holder h = new Holder();
    		h.background = (ImageView) v.findViewById(R.id.background);
            h.logo = (ImageView) v.findViewById(R.id.logo);
            h.name = (TextView) v.findViewById(R.id.name);
    		return h;
		}

		private static class Holder extends DrawerItemHolder {
			ImageView background;
			ImageView logo;
			TextView name;
			public Holder() {}
		}
	}
	
	public static class SearchItem extends DrawerItem implements
			OnClickListener {
		
		public SearchItem() {
			super(TYPE_SEARCH,null);
			resource = R.layout.drawer_item_search;
		}
		
		@Override
		public void getView(DrawerItemHolder holder, boolean selected) {
			Holder h = (Holder) holder;
			h.icon.setOnClickListener(this);
		}

		@Override
		public DrawerItemHolder getHolder(View v) {
			Holder h = new Holder();
    		h.search = (EditText) v.findViewById(R.id.search);
            h.icon = (IconTextView) v.findViewById(R.id.icon);
    		return h;
		}
		
		private static class Holder extends DrawerItemHolder {
    		IconTextView icon;
			EditText search;
			public Holder() {}
		}

		@Override
		public void onClick(View v) {
			Holder h = (Holder) v.getTag();
		}
	}
	
	public static class TextItem extends DrawerItem {
    	public int title;
    	String icon;
    	
    	public TextItem(int t, String i, Class f) {
			super(TYPE_TEXT,f);
			resource = R.layout.drawer_item;
			title = t;
			icon = i;
		}
    	
    	@Override
    	public DrawerItemHolder getHolder(View v) {
    		Holder h = new Holder();
    		h.container = (RelativeLayout) v.findViewById(R.id.container);
    		h.title = (TextView) v.findViewById(R.id.title);
            h.icon = (IconTextView) v.findViewById(R.id.icon);
    		return h;
    	}
    	
    	@Override
    	public void getView(DrawerItemHolder holder, boolean sel) {
    		Holder h = (Holder) holder;
    		h.icon.setText(icon);
            h.title.setText(title);
            
            if (sel) {
            	h.icon.setTextColor(ActivityMain.res.getColor(R.color.darkblue));
            } else {
            	h.icon.setTextColor(ActivityMain.res.getColor(R.color.drawer_item));
            }
    	}
    	
    	private static class Holder extends DrawerItemHolder {
    		RelativeLayout container;
			TextView title;
			IconTextView icon;
			public Holder() {}
		}
    }
}
