package com.tesladocet.thebuggenie.ui;

import android.support.v4.app.Fragment;
import android.view.View;

import com.tesladocet.thebuggenie.R;

public abstract class DrawerItem {
	
	public final static int TYPE_TEXT = 0;
	public final static int TYPE_SPINNER = 1;
	public final static int TYPE_GROUP = 2;
	public final static int TYPE_SEARCH = 3;
	
	public String id = null;
	int type = TYPE_TEXT;
	Class fragment = null;
	public int resource = R.layout.drawer_item;
	
	public DrawerItem() {
	}
	
	public DrawerItem(int type, Class f) {
		this.type = type;
		
		if (f != null && Fragment.class.isAssignableFrom(f)) {
			fragment = f;
		}
	}
	
    public abstract void getView(DrawerItemHolder h, boolean selected);
    public abstract DrawerItemHolder getHolder(View v);

    public static class DrawerItemHolder {}
}