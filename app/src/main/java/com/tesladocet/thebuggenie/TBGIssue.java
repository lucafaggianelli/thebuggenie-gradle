package com.tesladocet.thebuggenie;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.content.Context;
import android.graphics.Color;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.IconTextView;
import android.widget.TextView;

import com.google.gson.annotations.SerializedName;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class TBGIssue {
	
	public final static String EXTRA_PROJECT = "extra.project";
	private final static String DATE_FORMAT = "dd MMM ''yy";
	public final static long FIVE_DAYS = 432000000;
	
	int id;
	String title = null;
	String description = null;
    int state = 0;

    @SerializedName("issue_no")
    String type = null;
    
    String priority = null;
    
    //@SerializedName("issue_no")
    String issueNo = null;
    
    @SerializedName("posted_by")
    String postedBy = null;
    
    @SerializedName("assigned_to")
    String assignedTo = null;
    
    String status = null;
    
    @SerializedName("created_at")
    long createdAt = 0;
    
    @SerializedName("last_updated")
    long lastUpdated = 0;
    
    
	public TBGIssue() {
	}

    public int getPriorityColor() {
        if (priority == null)
            return 0;

        if (priority.equals("Low")) {
            return Color.GREEN;
        } else if (priority.equals("Critical")) {
            return Color.RED;
        } else if (priority.equals("Normal")) {
            return Color.GRAY;
        } else {
            return Color.GRAY;
        }
    }

	public String getPriorityResource() {
        if (priority == null)
            return null;

		if (priority.equals("Low")) {
			return "{fa_arrow_down}";
		} else if (priority.equals("Critical")) {
			return "{fa_arrow_up}";
		} else if (priority.equals("Normal")) {
			return "{fa_ellipsis_h}";
		} else {
			return "{fa_question}";
		}
	}

    public String getTypeResource() {
        if (type == null)
            return null;

        if (type.startsWith("Bug report")) {
            return "{fa_bug}";
        } else if (type.startsWith("Enhancement")) {
            return "{fa_lightbulb_o}";
        } else if (type.startsWith("Feature request")) {
            return "{fa_star}";
        } else {
            return "{fa_question}";
        }
    }
	
	static class Adapter extends ArrayAdapter<TBGIssue> {
		Context context;
		static ArrayList<TBGIssue> issues = new ArrayList<TBGIssue>();
		SimpleDateFormat dateFormat;
		long now;
		
		public Adapter(Context context) {
			super(context, R.layout.list_item_issue, issues);
			this.context = context;
			dateFormat = new SimpleDateFormat(DATE_FORMAT);
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Holder h;
			TBGIssue issue = getItem(position);
			
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) context
				        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			    convertView = inflater.inflate(R.layout.list_item_issue, parent, false);
			    h = new Holder(convertView);
			    convertView.setTag(h);
			} else {
				h = (Holder) convertView.getTag();
			}
			
			h.id.setText("#"+issue.id);
			h.title.setText(issue.title);
			h.assignedTo.setText(issue.assignedTo);
			h.lastUpdated.setText(getTime(issue.lastUpdated * 1000));

            if (issue.priority != null) {
                h.priority.setText(issue.getPriorityResource());
                h.priority.setTextColor(issue.getPriorityColor());
            }

            if (issue.type != null) {
                h.type.setText(issue.getTypeResource());
            }

			return convertView;
		}

        private String getTime(long time) {
			now = System.currentTimeMillis();
			
			if ((now - time) < FIVE_DAYS) {
				return DateUtils.getRelativeTimeSpanString(time).toString();
			} else {
				return dateFormat.format(new Date(time));
			}
		}
		
		class Holder {
            @InjectView(R.id.id) TextView id;
            @InjectView(R.id.title) TextView title;
            @InjectView(R.id.assigned_to) TextView assignedTo;
            @InjectView(R.id.last_updated) TextView lastUpdated;
            @InjectView(R.id.type) IconTextView type;
            @InjectView(R.id.priority) IconTextView priority;
			
			public Holder(View v) {
                ButterKnife.inject(this, v);
			}
		}
	}
}