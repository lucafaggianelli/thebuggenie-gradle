package com.tesladocet.thebuggenie;

import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class TBGProject {
	String id = null;
	public String name = null;
    public String image;
	
	public TBGProject(String id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public static TBGProject[] fromJson(JSONObject j) {
		if (j == null || j.length() == 0)
			return new TBGProject[]{};
		
		TBGProject[] projects = new TBGProject[j.length()];
		int i = 0;
		String id = null;
		Iterator<String> it = j.keys();
		
		try {
			while (it.hasNext()) {
				id = it.next();
				projects[i++] = new TBGProject(id, j.getString(id));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return projects;
	}
	
	static TBGProject findById(TBGProject[] projects, String id) {
		for (TBGProject p: projects) {
			if (p.id.equals(id)) return p;
		}
		return null;
	}
	
	static class Adapter extends ArrayAdapter<TBGProject> {
		Context context;
		
		public Adapter(Context context) {
			super(context, R.layout.list_item_issue);
			this.context = context;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) context
				        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			    convertView = inflater.inflate(R.layout.list_item_project, parent, false);
			    
				TBGProject project = getItem(position);
				((TextView)convertView.findViewById(R.id.name)).setText(project.name);
			}
			
			return convertView;
		}
	}
}