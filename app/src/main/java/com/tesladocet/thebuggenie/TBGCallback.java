package com.tesladocet.thebuggenie;

public interface TBGCallback<T> {
	void onSuccess(T result);
	void onError();
}
