package com.tesladocet.thebuggenie;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * An activity representing a list of Issues. This activity has different
 * presentations for handset and tablet-size devices. On handsets, the activity
 * presents a list of items, which when touched, lead to a
 * {@link IssueDetailActivity} representing item details. On tablets, the
 * activity presents the list of items and item details side-by-side using two
 * vertical panes.
 * <p>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link IssueListFragment} and the item details (if present) is a
 * {@link IssueDetailFragment}.
 * <p>
 * This activity also implements the required
 * {@link IssueListFragment.Callbacks} interface to listen for item selections.
 */
public class IssueListFragmentMain extends Fragment implements
		IssueListFragment.Callbacks {

	private String project;
	private static View rootView;
	
	/**
	 * Whether or not the activity is in two-pane mode, i.e. running on a tablet
	 * device.
	 */
	private boolean mTwoPane;
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {
		/*
		if (rootView != null) {
	        ViewGroup parent = (ViewGroup) rootView.getParent();
	        if (parent != null)
	            parent.removeView(rootView);
	    }*/
		
		try {
			rootView = inflater.inflate(
					R.layout.activity_issue_list, container, false);
		} catch (InflateException e) {
			e.printStackTrace();
		}

        for (Fragment f: getActivity().getSupportFragmentManager().getFragments()) {
            Log.d("IssueList","frag id "+f.getId());
        }

		IssueListFragment frag = (IssueListFragment) getChildFragmentManager()
				.findFragmentById(R.id.issue_list);
		frag.setCallback(this);

		if (rootView.findViewById(R.id.issue_detail_container) != null) {
			// The detail container view will be present only in the
			// large-screen layouts (res/values-large and
			// res/values-sw600dp). If this view is present, then the
			// activity should be in two-pane mode.
			mTwoPane = true;

			// In two-pane mode, list items should be given the
			// 'activated' state when touched.
			frag.setActivateOnItemClick(true);
		}

		if (getArguments() != null) {
			project = getArguments().getString("prj");
			if (project != null) {
				frag.setupIssues(project);
			}
		}

		return rootView;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// TODO: If exposing deep links into your app, handle intents here.
	}

	/**
	 * Callback method from {@link IssueListFragment.Callbacks} indicating that
	 * the item with the given ID was selected.
	 */
	@Override
	public void onItemSelected(int issueId) {
		if (mTwoPane) {
			// In two-pane mode, show the detail view in this activity by
			// adding or replacing the detail fragment using a
			// fragment transaction.
			Bundle arguments = new Bundle();
			arguments.putInt(IssueDetailFragment.ARG_ITEM_ID, issueId);
			arguments.putString("prj", project);
			IssueDetailFragment fragment = new IssueDetailFragment();
			fragment.setArguments(arguments);
			getActivity().getSupportFragmentManager().beginTransaction()
					.replace(R.id.issue_detail_container, fragment).commit();

		} else {
			// In single-pane mode, simply start the detail activity
			// for the selected item ID.
			Intent detailIntent = new Intent(getActivity(), IssueDetailActivity.class);
			detailIntent.putExtra("prj", project);
			detailIntent.putExtra(IssueDetailFragment.ARG_ITEM_ID, issueId);
			startActivity(detailIntent);
		}
	}
}
