package com.tesladocet.thebuggenie;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;

public class NotificationService extends IntentService {

	TBG tbg;
	NotificationManager notifier;
	int id = 0;
	
	public NotificationService() {
		super("TBGNotifier");
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		tbg = TBG.newInstace(this);
		
		// If there is no account set, cant read notifications
		if (!tbg.isConfigured()) {
			stopSelf();
			return;
		}
		
		notifier = (NotificationManager) getSystemService(
				Context.NOTIFICATION_SERVICE);
	}

	private void showNotification() {
		NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
			.setContentTitle("Luca")
			.setSmallIcon(android.R.drawable.btn_star)
			.setContentText("Closed bug #5");
		
		notifier.notify(id++, builder.build());
	}

	private void pollNotifications() {
		
	}
	
	@Override
	protected void onHandleIntent(Intent intent) {
		showNotification();
	}
	
	public static class BootReceiver extends BroadcastReceiver {
		@Override
	    public void onReceive(Context context, Intent intent) {
			Intent i = new Intent(context, NotificationService.class);
			context.startService(i);
	    }
	}
}