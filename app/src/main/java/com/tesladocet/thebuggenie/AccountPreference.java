package com.tesladocet.thebuggenie;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Message;
import android.preference.DialogPreference;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.EditText;

public class AccountPreference extends DialogPreference {

	SharedPreferences pref;
	TBG tbg;
	ProgressDialog progressDialog;
	EditText userField;
	EditText passField;
	
	public AccountPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	
	public AccountPreference(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}
	
	@Override
	protected void onAttachedToHierarchy(PreferenceManager preferenceManager) {
		super.onAttachedToHierarchy(preferenceManager);
		pref = getSharedPreferences();
        String u = pref.getString(SettingsActivity.KEY_USER, null);
        String p = pref.getString(SettingsActivity.KEY_PASSWORD, null);
        
        if (u != null && p != null)
        	setSummary(u);

	}
	
	private void init() {
		setDialogLayoutResource(R.layout.account_preference);
        setPositiveButtonText(android.R.string.ok);
        setNegativeButtonText(android.R.string.cancel);
        setDialogIcon(null);
        
        tbg = TBG.getInstance();
	}
	
	private ProgressDialog loginProgress() {
		ProgressDialog dialog;

		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
		   dialog = new ProgressDialog(new ContextThemeWrapper(getContext(), android.R.style.Theme_Holo_Light_Dialog));
		} else {
		   dialog = new ProgressDialog(getContext());
		}
		
		dialog.setTitle("Logging in...");
		dialog.setIndeterminate(true);
		dialog.show();
		
		return dialog;
	}
	
	@Override
	protected void onBindDialogView(View view) {
		super.onBindDialogView(view);
		
		userField = (EditText) view.findViewById(R.id.user);
		passField = (EditText) view.findViewById(R.id.password);
		
		userField.setText(pref.getString(SettingsActivity.KEY_USER, ""));
	}
	
	@Override
	protected void onSetInitialValue(boolean restorePersistedValue,
			Object defaultValue) {
		
		Log.d("Pref", "called");
		
		SharedPreferences pref = getSharedPreferences();
		if (restorePersistedValue) {
			userField.setText(pref.getString(SettingsActivity.KEY_USER, ""));
			//passField.setText(pref.getString(SettingsActivity.KEY_PASSWORD, ""));
		}
	}
	
	@Override
	protected void onDialogClosed(boolean positiveResult) {
		if (!positiveResult) return;

		final String user = userField.getText().toString();
		final String pass = passField.getText().toString();
		final SharedPreferences.Editor editor = getEditor();
		
		progressDialog = loginProgress();
		
		tbg.login(user, pass, new TBGCallback<Boolean>() {
			@Override
			public void onSuccess(Boolean useless) {
				handler.obtainMessage(1, user).sendToTarget();
			}
			
			@Override
			public void onError() {
				handler.obtainMessage(0).sendToTarget();
			}
		});
	}
	
	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (msg.what == 0) {
				progressDialog.setTitle("Can't login!");
				progressDialog.setButton(Dialog.BUTTON_NEGATIVE, "OK", new Message());
			} else if (msg.what == 1) {
				progressDialog.dismiss();
				setSummary((String)msg.obj);
			}
		}
	};
}
