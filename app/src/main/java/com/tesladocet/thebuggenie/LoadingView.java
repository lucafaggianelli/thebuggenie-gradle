package com.tesladocet.thebuggenie;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class LoadingView extends RelativeLayout {

	Context context;
	AttributeSet attrs = null;
	String textFail = null;
	String text = null;
	
	TextView textFailView;
	TextView textView;
	ProgressBar progress;
	RelativeLayout container;
	
	public LoadingView(Context context) {
		super(context);
		this.context = context;
		init();
	}
	
	public LoadingView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		this.attrs = attrs;
		init();
	}
	
	public LoadingView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
		this.attrs = attrs;
		init();
	}

	private void init() {
		inflate(context, R.layout.loading, this);
		
		textView = (TextView) findViewById(R.id.loading_text);
		textFailView = (TextView) findViewById(R.id.loading_fail);
		progress = (ProgressBar) findViewById(R.id.loading_progress);
		container = (RelativeLayout) findViewById(R.id.loading_container);
		container.setClickable(true);
		container.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {return true;}
		});
		
		if (attrs != null) {
			TypedArray a = context.getTheme().obtainStyledAttributes(
					attrs, R.styleable.LoadingView, 0, 0);
			
			try {
				textFail = a.getString(R.styleable.LoadingView_textFail);
			} finally {
				a.recycle();
			}
			
			textView.setText(text);
			
			textFailView.setText(textFail);
			textFailView.setVisibility(View.GONE);
		}
	}

    public void setText(String text) {
        this.text = text;
        textView.setText(text);
    }

    public void setTextFail(String textFail) {
        this.textFail = textFail;
        textFailView.setText(textFail);
    }

    public void setLoading() {
		textFailView.setVisibility(View.GONE);
		textView.setVisibility(View.VISIBLE);
		progress.setVisibility(View.VISIBLE);
		container.setVisibility(View.VISIBLE);
	}
	
	public void setFail() {
		textFailView.setVisibility(View.VISIBLE);
		textView.setVisibility(View.GONE);
		progress.setVisibility(View.GONE);
		container.setVisibility(View.VISIBLE);
	}
	
	public void setDone() {
		container.setVisibility(View.GONE);
	}
}
