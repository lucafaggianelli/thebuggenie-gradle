package com.tesladocet.thebuggenie;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

abstract public class Loader {

	private final static LayoutParams MATCH_PARENT =
			new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
	
	private Context context;
	private int tasksCount;
    private int tasksDone = 0;
    private boolean failed = false;
	private LoadingView loading;
	
	public Loader(View v, int count) {
		context = v.getContext();
		tasksCount = count;
		loading = new LoadingView(context);
		loading.setLayoutParams(MATCH_PARENT);
		loading.setLoading();
		
		if (v instanceof ViewGroup) {
			((ViewGroup) v).addView(loading);
		}
	}

    public LoadingView getLoading() {
        return loading;
    }

    protected void taskDone() {
        taskDone(false);
    }

	protected void taskDone(boolean f) {
        this.failed = f;

        Log.d("Loading", "Remaining tasks: "+tasksCount);

		if (--tasksCount <= 0) {
            Log.d("Loading", "tasks done, failed?"+failed);
            if (failed) {
                loading.setFail();
            } else {
                loading.setDone();
            }
			onFinish(failed);
		}
	}
	
	abstract void onFinish(boolean failed);
}