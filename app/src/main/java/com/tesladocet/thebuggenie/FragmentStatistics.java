package com.tesladocet.thebuggenie;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FragmentStatistics extends Fragment {

	PieChart pieChart;
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_statistics, null);
	}
	
	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		pieChart = (PieChart) view.findViewById(R.id.pie);
		pieChart.addSlice(new PieChart.Slice("Close", 3, PieChart.GREEN));
		pieChart.addSlice(new PieChart.Slice("Oepn", 14, PieChart.RED));
	}
}
