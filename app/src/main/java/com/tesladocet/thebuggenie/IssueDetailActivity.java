package com.tesladocet.thebuggenie;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.widget.TextView;

/**
 * An activity representing a single Issue detail screen. This activity is only
 * used on handset devices. On tablet-size devices, item details are presented
 * side-by-side with a list of items in a {@link IssueListFragmentMain}.
 * <p>
 * This activity is mostly just a 'shell' activity containing nothing more than
 * a {@link IssueDetailFragment}.
 */
public class IssueDetailActivity extends ActionBarActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_issue_detail);

		// Show the Up button in the action bar.
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		// not null after rotation
		if (savedInstanceState == null) {
			// Create the detail fragment and add it to the activity
			// using a fragment transaction.
			Bundle arguments = new Bundle();
			arguments.putInt(IssueDetailFragment.ARG_ITEM_ID, getIntent()
					.getIntExtra(IssueDetailFragment.ARG_ITEM_ID,-1));
			arguments.putString("prj", getIntent().getStringExtra("prj"));
			IssueDetailFragment fragment = new IssueDetailFragment();
			fragment.setArguments(arguments);
			getSupportFragmentManager().beginTransaction()
					.add(R.id.issue_detail_container, fragment).commit();
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			NavUtils.navigateUpTo(this, new Intent(this,
					IssueListFragmentMain.class));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
