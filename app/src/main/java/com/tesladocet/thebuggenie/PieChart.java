package com.tesladocet.thebuggenie;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Path.Direction;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region.Op;
import android.util.ArrayMap;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class PieChart extends View {

	public final static int GRAY 	= 0xFF4D4D4D;
	public final static int BLUE 	= 0xFF5DA5DA;
	public final static int ORANGE 	= 0xFFFAA43A;
	public final static int GREEN 	= 0xFF60BD68;
	public final static int PINK	= 0xFFF17CB0;
	public final static int BROWN 	= 0xFFB2912F;
	public final static int PURPLE	= 0xFFB276B2;
	public final static int YELLOW	= 0xFFDECF3F;
	public final static int RED		= 0xFFF15854;
	
	private Paint paintMain;
	private Paint paintBackground;
	private Paint paintText;
	private int diameter;
	private RectF bounds;
	private RectF boundsInner;
	private Rect textBounds = new Rect();
	private String percentage;
	private int strokeWidth;
	private float total = 0;
	
	// configurable
	int colorBack = 0xFFCCCCCC;
	float progress = 0.5f;
	Type type = Type.CAKE;
	float thickness = 0.1f;
	float shadowThickness = .65f;
	ArrayList<Slice> slices = new ArrayList<Slice>();
	
	public PieChart(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init();
	}

	public PieChart(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		TypedArray a = getContext().getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.PieChart,
                0, 0
        );
		
		try {
            progress = a.getFloat(R.styleable.PieChart_progress, 0.0f);
            type = Type.values()[a.getInt(R.styleable.PieChart_type, 0)];
        } finally {
            // release the TypedArray so that it can be reused.
            a.recycle();
        }
		
		init();
	}

	public PieChart(Context context) {
		super(context);
		init();
	}
	
	private void init() {
		if (isInEditMode()) {
			addSlice(new Slice("Sample1", 15, 0xFFFE6DA8));
			addSlice(new Slice("Sample1", 25, 0xFF56B7F1));
			addSlice(new Slice("Sample1",  8, 0xFFCDA67F));
			addSlice(new Slice("Sample1", 34, 0xFFFED70E));
		}
	
		paintMain = new Paint(Paint.ANTI_ALIAS_FLAG);
		paintMain.setStrokeWidth(strokeWidth);
		if (type == Type.CAKE || type == Type.DONUT) {
			paintMain.setStyle(Paint.Style.FILL);
		} else {
			paintMain.setStyle(Paint.Style.STROKE);
		}
		
		paintBackground = new Paint(Paint.ANTI_ALIAS_FLAG);
		if (type == Type.DONUT)
			paintBackground.setColor(Color.WHITE);
		else
			paintBackground.setColor(colorBack);
		paintBackground.setStyle(Paint.Style.FILL);
		
		paintText = new Paint(Paint.ANTI_ALIAS_FLAG);
		paintText.setColor(0xFF404040);
		paintText.setTextAlign(Align.CENTER);
		paintText.setTextSize(20);
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		
		int xpad = getPaddingLeft() + getPaddingRight();
		int ypad = getPaddingTop() + getPaddingBottom();
		
		diameter = Math.min(w - xpad, h - ypad);
		strokeWidth = (int) (diameter * thickness);
		paintMain.setStrokeWidth(strokeWidth);
		diameter -= strokeWidth;
		
		bounds = new RectF(0f, 0f, diameter, diameter);
        bounds.offsetTo(getPaddingLeft() + strokeWidth/2,
        		getPaddingTop() + strokeWidth/2);
        
        boundsInner = new RectF(0f, 0f, diameter*shadowThickness, diameter*shadowThickness);
        boundsInner.offsetTo(bounds.centerX()-boundsInner.width()/2,
        		bounds.centerY()-boundsInner.height()/2);
        
        paintText.setTextSize(diameter / 2);
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
	
		if (type == Type.PERCENT) {
			canvas.drawArc(bounds, -90, 360 * progress, false, paintMain);
	
			canvas.drawCircle(bounds.centerX(), bounds.centerY(), 
					diameter/2, paintBackground);
			
			percentage = (int)(progress*100)+"";
			paintText.getTextBounds(percentage, 0, percentage.length(), textBounds);
			canvas.drawText(percentage, bounds.centerX(), 
					bounds.centerY()+textBounds.height()/2, paintText);
			
		} else if (type == Type.DONUT) {
			float startAngle = -90;
			float sweepAngle = 0;
			Slice slice;

			for (int i = 0; i < slices.size(); i++) {
				slice = slices.get(i);
				sweepAngle = (float) Math.ceil(360*slice.value/total);
				
				paintMain.setColor(slice.color);
				canvas.drawArc(bounds, startAngle, sweepAngle, true, paintMain);
				paintMain.setColor(adjustColor(slice.color, 1.1f));
				canvas.drawArc(boundsInner, startAngle, sweepAngle, true, paintMain);
				
				startAngle += sweepAngle;
			}
			canvas.drawCircle(bounds.centerX(), bounds.centerY(), 
					bounds.height()*0.29f, paintBackground);
		}
	}
	
	public void addSlice(Slice s) {
		slices.add(s);
		update();
	}
	
	public void clearSlices() {
		slices.clear();
		update();
	}
	
	private int adjustColor(int color, float factor) {
		int r = Math.min((int) (((color >> 16) & 0xFF) * factor), 0xFF);
		int g = Math.min((int) (((color >>  8) & 0xFF) * factor), 0xFF);
		int b = Math.min((int) (((color >>  0) & 0xFF) * factor), 0xFF);
		
		return Color.argb((color >> 24) & 0xFF, r, g, b);
	}
	
	private void update() {
		total = 0;
		Slice s;
		for (int i=0; i<slices.size(); i++) {
			s = slices.get(i);
			total += s.value;
		}
		invalidate();
		//total += slices.get(slices.size()-1).value;
	}
	
	public static class Slice {
		int value;
		int color;
		String label;
		int resource;
		
		public Slice(String l, int v, int c) {
			label = l;
			value = v;
			color = c;
		}
	}
	
	public enum Type {
		CAKE,
		DONUT,
		PERCENT,
	}
}