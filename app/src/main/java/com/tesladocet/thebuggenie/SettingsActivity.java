package com.tesladocet.thebuggenie;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

public class SettingsActivity extends PreferenceActivity implements
		OnPreferenceChangeListener {

	SharedPreferences preferences;
	Resources res;
	TBG tbg;
	
	public static String KEY_SERVER = "server";
	public static String KEY_USER = "user";
	public static String KEY_PASSWORD = "pass";
	public static String KEY_CURRENT_PROJECT = "project_current";
	
	public static int REQUEST_SERVER_SETUP = 0;
	
	public static int RESULT_SERVER_NOT_SETUP = 0;
	public static int RESULT_SERVER_SETUP = 1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		res = getResources();
		tbg = TBG.getInstance();
		
		addPreferencesFromResource(R.xml.preferences);
		findPreference(res.getString(R.string.pref_k_version)).setSummary(""+tbg.tbgVersion[0]);
		findPreference(res.getString(R.string.pref_k_api_version)).setSummary(""+tbg.apiVersion);
		findPreference(res.getString(R.string.pref_k_api_module)).setSummary(tbg.restApiModule?"Installed":"Not installed");
	}

	@Override
	public boolean onPreferenceChange(Preference preference, Object newValue) {
		String k = preference.getKey();
		
		if (k.equals(KEY_SERVER)) {
			try {
				new URL((String) newValue);
				setResult(RESULT_SERVER_SETUP);
			} catch (MalformedURLException e) {
				final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Not a valid URL!");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.show();
				return false;
			}
		}
		
		return true;
	}
}