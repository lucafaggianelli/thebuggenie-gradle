package com.tesladocet.thebuggenie;

import java.util.Map;

public class TBGFieldValues {

	public String description;
	public String type;
	public Map<String,String> choices;
}