package com.tesladocet.thebuggenie;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.loopj.android.http.RequestParams;

import java.lang.annotation.Annotation;

public class Form {

    int layout;

    public Form(int layout) {
        this.layout = layout;
    }


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(layout,null);
    }


    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        if (!(view instanceof ViewGroup)) return;

        ViewGroup v = (ViewGroup) view;
        View input;
        String value;
        for (int i=0; i<v.getChildCount(); i++) {
            input = v.getChildAt(i);

            if (input instanceof EditText) {
                value = ((EditText) input).getText().toString();
            } else if (input instanceof Spinner) {
                value = ""+((Spinner) input).getSelectedItemPosition();
            } else if (input instanceof RadioGroup) {
                value = ""+((RadioGroup) input).getCheckedRadioButtonId();
            }
        }
    }

    private RequestParams serialize(ViewGroup v) {
        if (v == null)
            return null;

        RequestParams params = new RequestParams();

        View input;
        String value;
        for (int i=0; i<v.getChildCount(); i++) {
            input = v.getChildAt(i);

            if (input instanceof EditText) {
                value = ((EditText) input).getText().toString();
            } else if (input instanceof Spinner) {
                value = ""+((Spinner) input).getSelectedItemPosition();
            } else if (input instanceof RadioGroup) {
                value = ""+((RadioGroup) input).getCheckedRadioButtonId();
            } else {
                continue;
            }
            params.put((String)v.getTag(), value);
        }

        return params;
    }
}
