package com.tesladocet.thebuggenie;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class FormIssue extends Dialog implements Validator.ValidationListener {

    @InjectView(R.id.type)
    Spinner type;

    @NotEmpty
    @InjectView(R.id.title)
    EditText title;

    @InjectView(R.id.description)
    EditText description;

    @InjectView(R.id.priority)
    Spinner priority;

    @InjectView(R.id.resolution)
    Spinner resolution;

    @InjectView(R.id.release)
    Spinner release;

    @InjectView(R.id.complete)
    SeekBar percentComplete;

    Context context;
    Validator validator;

    public FormIssue(Context context) {
        super(context);
        this.context = context;

        setContentView(R.layout.form_issue_create);

        ButterKnife.inject(this);
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    @Override
    public void onValidationSucceeded() {
        RequestParams params = new RequestParams();

        params.put("title", title.getText().toString());
        params.put("issuetype", type.getSelectedItemPosition());
        params.put("description", description.getText().toString());
        params.put("priority", priority.getSelectedItemPosition());
        params.put("resolution", resolution.getSelectedItemPosition());
        params.put("build_id", release.getSelectedItemPosition());
        params.put("priority_id", priority.getSelectedItemPosition());
        params.put("percent_complete", percentComplete.getProgress());
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

    }

    @OnClick(R.id.submit)
    public void onSubmit() {
        validator.validate();
    }
}