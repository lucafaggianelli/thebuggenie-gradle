// Generated code from Butter Knife. Do not modify!
package com.tesladocet.thebuggenie;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class TBGIssue$Adapter$Holder$$ViewInjector<T extends com.tesladocet.thebuggenie.TBGIssue.Adapter.Holder> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131361887, "field 'id'");
    target.id = finder.castView(view, 2131361887, "field 'id'");
    view = finder.findRequiredView(source, 2131361834, "field 'title'");
    target.title = finder.castView(view, 2131361834, "field 'title'");
    view = finder.findRequiredView(source, 2131361863, "field 'assignedTo'");
    target.assignedTo = finder.castView(view, 2131361863, "field 'assignedTo'");
    view = finder.findRequiredView(source, 2131361862, "field 'lastUpdated'");
    target.lastUpdated = finder.castView(view, 2131361862, "field 'lastUpdated'");
    view = finder.findRequiredView(source, 2131361879, "field 'type'");
    target.type = finder.castView(view, 2131361879, "field 'type'");
    view = finder.findRequiredView(source, 2131361881, "field 'priority'");
    target.priority = finder.castView(view, 2131361881, "field 'priority'");
  }

  @Override public void reset(T target) {
    target.id = null;
    target.title = null;
    target.assignedTo = null;
    target.lastUpdated = null;
    target.type = null;
    target.priority = null;
  }
}
