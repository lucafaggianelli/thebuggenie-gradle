// Generated code from Butter Knife. Do not modify!
package com.tesladocet.thebuggenie;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.Injector;

public class FormIssue$$ViewInjector<T extends com.tesladocet.thebuggenie.FormIssue> implements Injector<T> {
  @Override public void inject(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131361879, "field 'type'");
    target.type = finder.castView(view, 2131361879, "field 'type'");
    view = finder.findRequiredView(source, 2131361834, "field 'title'");
    target.title = finder.castView(view, 2131361834, "field 'title'");
    view = finder.findRequiredView(source, 2131361880, "field 'description'");
    target.description = finder.castView(view, 2131361880, "field 'description'");
    view = finder.findRequiredView(source, 2131361881, "field 'priority'");
    target.priority = finder.castView(view, 2131361881, "field 'priority'");
    view = finder.findRequiredView(source, 2131361882, "field 'resolution'");
    target.resolution = finder.castView(view, 2131361882, "field 'resolution'");
    view = finder.findRequiredView(source, 2131361883, "field 'release'");
    target.release = finder.castView(view, 2131361883, "field 'release'");
    view = finder.findRequiredView(source, 2131361884, "field 'percentComplete'");
    target.percentComplete = finder.castView(view, 2131361884, "field 'percentComplete'");
    view = finder.findRequiredView(source, 2131361878, "method 'onSubmit'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.onSubmit();
        }
      });
  }

  @Override public void reset(T target) {
    target.type = null;
    target.title = null;
    target.description = null;
    target.priority = null;
    target.resolution = null;
    target.release = null;
    target.percentComplete = null;
  }
}
